//
//  ViewController.swift
//  activity1_c0741753
//
//  Created by Shashank Machani on 2019-10-27.
//  Copyright © 2019 Keerthi. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController:UIViewController,WCSessionDelegate{
   
    @IBOutlet weak var wakeup: UIButton!
    @IBOutlet weak var choselabel: UILabel!
    @IBOutlet weak var pikachu: UIButton!
    var session:WCSession!
    @IBOutlet weak var caterpie: UIButton!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
  
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        DispatchQueue.main.async {
            var pikaname = message["pikaname"] as! String
            var name = message["name"] as! String
            self.choselabel.text = "\(name)is hybernating"
            if(pikaname == "pikachu"){
                self.caterpie.isHidden = true
            }
            else if(pikaname == "caterpie"){
                self.pikachu.isHidden = true
            }
            self.wakeup.isHidden = false
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (WCSession.isSupported()) {
            print("PHONE: Phone supports WatchConnectivity!")
            self.session = WCSession.default
            self.session.delegate = self
            self.session.activate()
        }
        else {
            print("PHONE: Phone does not support WatchConnectivity")
        }
        self.wakeup.isHidden = true
        
    }
    
    @IBAction func pikachupressed(_ sender: Any) {
        let message = ["name":"pikachu"]
        WCSession.default.sendMessage(message, replyHandler: nil)
        
    }
    
    @IBAction func caterpiepressed(_ sender: Any) {
        let message = ["name":"caterpie"]
        WCSession.default.sendMessage(message, replyHandler: nil)
    }
    
}
