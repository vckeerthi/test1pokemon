//
//  InterfaceController.swift
//  activity1_c0741753 WatchKit Extension
//
//  Created by Shashank Machani on 2019-10-27.
//  Copyright © 2019 Keerthi. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController,WCSessionDelegate {
    
    @IBOutlet weak var pikaimage: WKInterfaceImage!

    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    var name : String!
    var pikaname: String!
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        WKInterfaceDevice().play(.click)
       var msg = message["name"] as! String
        if (msg == "pikachu"){
            pikaimage.setImageNamed("pikachu")
           pikaname = msg
        }
        else if (msg == "caterpie"){
            pikaimage.setImageNamed("caterpie")
            pikaname = msg
         
        }
    }
    

    
    override func willActivate() {
        super.willActivate()
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        
            
        }
    }
    
    @IBAction func startgamePressed() {
        var names = [(self.name)!, (self.pikaname)!]
        pushController(withName: "SecondView", context: names)
        
    }
//    override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
//        let segue = segueIdentifier as! String
//        if(segue == "segue"){
//        return (self.name)!
//            print((self.name)!)
//        }
//        return nil
//    }
    @IBAction func enterednamePressed() {
        let cannedResponses = ["jenelle", "albert", "pritesh", "mohammad"]
        presentTextInputController(withSuggestions: cannedResponses, allowedInputMode: .plain) {
            (results) in
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.nameLabel.setText(userResponse)
                self.name = userResponse!
                
            }
        }
    }
    
    
}
