//
//  SecondView.swift
//  activity1_c0741753 WatchKit Extension
//
//  Created by Shashank Machani on 2019-10-31.
//  Copyright © 2019 Keerthi. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity



class SecondView: WKInterfaceController,WCSessionDelegate{
    var name :String!
    var pikaname: String!
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    @IBOutlet weak var hungerLabel: WKInterfaceLabel!
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
      if let val: [String] = context as? [String] {
        print(val)
            self.nameLabel.setText("\(val[0]) is hungry")
        self.name = val[0]
        self.pikaname = val[1]
        } else {
            self.nameLabel.setText("no name")
        print("no name")
        
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func hybernatePressed() {
        let message = ["name":"\(self.name)","pikaname":"\(self.pikaname)"]
        WCSession.default.sendMessage(message, replyHandler: nil)
        }
    
    @IBAction func feedPressed() {
    }
}
